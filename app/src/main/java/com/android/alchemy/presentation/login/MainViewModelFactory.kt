package com.android.alchemy.presentation.login


import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.android.alchemy.data.repository.interactor.OnBoardingRepository
import io.reactivex.disposables.CompositeDisposable

@Suppress("UNCHECKED_CAST")
class MainViewModelFactory(
    private val onBoardingRepository: OnBoardingRepository.Repository,
    private val compositeDisposable: CompositeDisposable
) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return MainViewModel(onBoardingRepository, compositeDisposable) as T
    }
}