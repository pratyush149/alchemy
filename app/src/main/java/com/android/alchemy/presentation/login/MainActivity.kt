package com.android.alchemy.presentation.login

import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.alchemy.R
import com.android.alchemy.common.NetworkUtil
import com.android.alchemy.data.remote.model.response.ProductResponse
import com.android.alchemy.di.DaggerComponentProvider
import com.android.alchemy.presentation.base.BaseActivity
import com.android.alchemy.presentation.userlist.AddProductActivity
import com.ee.core.networking.Outcome
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_login.*
import javax.inject.Inject

class MainActivity : BaseActivity() {

    private val mComponent by lazy { DaggerComponentProvider.onBoardingComponent() }

    private val mViewModel: MainViewModel by lazy {
        ViewModelProviders.of(this, mViewModelFactory).get(MainViewModel::class.java)
    }

    private val TAG = MainActivity::class.java.simpleName

    @Inject
    lateinit var mSharedPreferences: SharedPreferences

    @Inject
    lateinit var mViewModelFactory: MainViewModelFactory

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        mComponent.inject(this)

        initObserver()

        setRecycler()

        initClickListeners()


    }

    override fun onResume() {
        super.onResume()
        if (NetworkUtil.isNetworkAvailable(this)) {
            callGetProducts()
        } else {
            showSnackBar("Can't connect to Internet")
        }
    }

    private fun initClickListeners() {
        fab_add.setOnClickListener {
            launchNextScreen()
        }
    }

    private fun setRecycler() {
        rvMain.layoutManager = LinearLayoutManager(this)
    }


    private fun initObserver() {

        initProductResponseObserver()
    }

    private fun initProductResponseObserver() {

        mViewModel.mProductResponse.observe(
            this,
            Observer<Outcome<List<ProductResponse>>> { outcome ->

                when (outcome) {

                    is Outcome.Progress -> {
                        showProgressDialog()
                    }

                    is Outcome.Success -> {
                        cancelProgressDialog()
                        rvMain.adapter = MainRecyclerAdapter(this, outcome.data)

//                    saveLoginDataToSharedPref(outcome.data)
//                    launchNextScreen()
                    }

                    is Outcome.Failure -> {
                        cancelProgressDialog()
                    }
                }
            })

        mViewModel.mProductDbResponse.observe(this, Observer {
            Log.d(TAG, "initLoginResponseObserver: got data from db"+ it.size)
        })
    }



    private fun callGetProducts() {
        mViewModel.getProducts()
    }

    private fun launchNextScreen() {

        startActivity(Intent(this, AddProductActivity::class.java))

    }

    private fun showSnackBar(title: String) {
        val snackBar =
            Snackbar.make(findViewById(android.R.id.content), title, Snackbar.LENGTH_LONG)
        val snackBarView = snackBar.view
        snackBarView.setBackgroundColor(ContextCompat.getColor(this, R.color.white))

        val textView =
            snackBarView.findViewById(com.google.android.material.R.id.snackbar_text) as TextView

        textView.setTextColor(Color.RED)
        textView.textSize = 20f
        snackBar.show()
    }

}