package com.android.alchemy.presentation.login

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.android.alchemy.R
import com.android.alchemy.common.GlideApp
import com.android.alchemy.data.remote.model.response.ProductResponse

class MainRecyclerAdapter(val mContext: Context, val data: List<ProductResponse>) :
    RecyclerView.Adapter<MainRecyclerAdapter.MainViewHolder>() {

    inner class MainViewHolder(
        itemView: View
    ) : RecyclerView.ViewHolder(itemView) {
        var ivImage: ImageView
        var tvHeader: TextView
        var tvDesc: TextView
        var tvPrice: TextView
        var tvAdd: TextView
        var cvInterest: CardView

        init {
            ivImage = itemView.findViewById(R.id.ivImage)
            tvHeader = itemView.findViewById(R.id.tvHeader)
            tvDesc = itemView.findViewById(R.id.tvDesc)
            tvPrice = itemView.findViewById(R.id.tvPrice)
            tvAdd = itemView.findViewById(R.id.tvAdd)
            cvInterest = itemView.findViewById(R.id.cvInterest)

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MainViewHolder {
        val view = LayoutInflater.from(mContext)
            .inflate(R.layout.item_products, parent, false)
        return MainViewHolder(view)
    }

    override fun onBindViewHolder(holder: MainViewHolder, position: Int) {
        val productData: ProductResponse = data.get(position)
        holder.tvHeader.text = productData.name
        val desc = productData.adjective + " " + productData.material
        holder.tvDesc.text = desc
        val price = "₹ " + productData.price
        holder.tvPrice.text = price
        GlideApp.with(mContext).load(productData.image).into(holder.ivImage)
    }

    override fun getItemCount(): Int {
        return data.size
    }
}