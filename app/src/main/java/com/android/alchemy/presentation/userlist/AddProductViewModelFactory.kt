package com.android.alchemy.presentation.userlist


import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.android.alchemy.data.repository.interactor.AddProductRepository
import io.reactivex.disposables.CompositeDisposable

@Suppress("UNCHECKED_CAST")
class AddProductViewModelFactory(
    private val userListRepository: AddProductRepository.Repository,
    private val compositeDisposable: CompositeDisposable
) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return AddProductViewModel(userListRepository, compositeDisposable) as T
    }
}