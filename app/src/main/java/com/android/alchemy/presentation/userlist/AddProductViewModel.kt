package com.android.alchemy.presentation.userlist

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.android.alchemy.data.remote.model.request.ProductRequest
import com.android.alchemy.data.remote.model.response.ProductResponse
import com.android.alchemy.data.repository.interactor.AddProductRepository
import com.android.alchemy.usecase.UserListUseCase
import com.ee.core.extension.toLiveData
import com.ee.core.networking.Outcome
import io.reactivex.disposables.CompositeDisposable

class AddProductViewModel(
    private val userListRepository: AddProductRepository.Repository,
    private val compositeDisposable: CompositeDisposable
) : ViewModel(), UserListUseCase {

    val mUserListResponse: LiveData<Outcome<ProductResponse>> by lazy {
        userListRepository.userListResponse.toLiveData(compositeDisposable)
    }

    override fun fetchUsers(body :ProductRequest) {
        userListRepository.addProduct(body)
    }

}

