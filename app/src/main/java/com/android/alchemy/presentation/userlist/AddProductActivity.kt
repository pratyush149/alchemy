package com.android.alchemy.presentation.userlist

import android.content.SharedPreferences
import android.graphics.Color
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProviders
import com.android.alchemy.R
import com.android.alchemy.data.remote.model.request.ProductRequest
import com.android.alchemy.data.remote.model.response.ProductResponse
import com.android.alchemy.di.DaggerComponentProvider
import com.android.alchemy.presentation.base.BaseActivity
import com.android.alchemy.common.KeyboardUtils
import com.android.alchemy.common.NetworkUtil
import com.ee.core.networking.Outcome
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_add_product.*
import javax.inject.Inject

class AddProductActivity : BaseActivity() {

    val TAG = AddProductActivity::class.java.simpleName
    private val mComponent by lazy { DaggerComponentProvider.userListComponent() }

    @Inject
    lateinit var mSharedPref: SharedPreferences

    @Inject
    lateinit var mViewModelFactory: AddProductViewModelFactory

    private val mUserListViewModel: AddProductViewModel by lazy {
        ViewModelProviders.of(this, mViewModelFactory).get(AddProductViewModel::class.java)
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_product)

        mComponent.inject(this)

        initObserver()

        setClickListeners()

    }


    private fun setClickListeners() {
        ivBack.setOnClickListener {
            onBackPressed()
        }
        btSubmit.setOnClickListener {
            KeyboardUtils.hideSoftKeyboard(this)
            if (validateAddProductData()) {
                if (NetworkUtil.isNetworkAvailable(this)) {
                    fetchUsersList()
                } else {
                    showSnackBar("Can't connect to Internet")
                }
            }
        }
    }

    private fun validateAddProductData(): Boolean {
        if (etUserName.text.toString().trim().isEmpty()) {
            showSnackBar("Please enter valid Name")
            return false
        }

        if (etColor.text.toString().trim().isEmpty()) {
            showSnackBar("Please enter valid Color")
            return false
        }

        if (etPrice.text.toString().trim().isEmpty()) {
            showSnackBar("Please enter valid Price")
            return false
        }

        if (etAdjective.text.toString().trim().isEmpty()) {
            showSnackBar("Please enter valid Adjective")
            return false
        }
        if (etMaterial.text.toString().trim().isEmpty()) {
            showSnackBar("Please enter valid Material")
            return false
        }

        return true
    }

    private fun initObserver() {

        mUserListViewModel.mUserListResponse.observe(
            this,
            androidx.lifecycle.Observer<Outcome<ProductResponse>> { outcome ->

                when (outcome) {

                    is Outcome.Progress -> {
                        showProgressDialog()
                    }
                    is Outcome.Success -> {
                        cancelProgressDialog()
                        Log.d(TAG, "initObserver: Product Created")
                        showSnackBar("Product Created Successfully")
                        Handler().postDelayed({
                            onBackPressed()

                        }, 2000)
                    }
                    is Outcome.Failure -> {
                        cancelProgressDialog()
                    }
                }
            })
    }

    private fun fetchUsersList() {
        val body = ProductRequest(
            etUserName.text.toString().trim(),
            etColor.text.toString().trim(),
            etPrice.text.toString().trim(),
            etAdjective.text.toString().trim(),
            etMaterial.text.toString().trim()
        )
        mUserListViewModel.fetchUsers(body)
    }


    private fun showSnackBar(title: String) {
        val snackBar =
            Snackbar.make(findViewById(android.R.id.content), title, Snackbar.LENGTH_LONG)
        val snackBarView = snackBar.view
        snackBarView.setBackgroundColor(ContextCompat.getColor(this, R.color.white))

        val textView =
            snackBarView.findViewById(com.google.android.material.R.id.snackbar_text) as TextView

        textView.setTextColor(Color.RED)
        textView.textSize = 20f
        snackBar.show()
    }

}

