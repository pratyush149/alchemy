package com.android.alchemy.presentation.login

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.android.alchemy.UsersApplication
import com.android.alchemy.data.remote.model.response.ProductResponse
import com.android.alchemy.data.repository.interactor.OnBoardingRepository
import com.android.alchemy.usecase.LoginUseCase
import com.android.kotlinmvvm.data.database.entity.Product
import com.ee.core.extension.toLiveData
import com.ee.core.networking.Outcome
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class MainViewModel(
    private val onBoardingRepository: OnBoardingRepository.Repository,
    private val compositeDisposable: CompositeDisposable
) : ViewModel(), LoginUseCase {

    val mProductResponse: LiveData<Outcome<List<ProductResponse>>> by lazy {
        onBoardingRepository.productResponse.toLiveData(compositeDisposable)
    }

    val mProductDbResponse: MutableLiveData<List<ProductResponse>> = MutableLiveData()

    override fun getProducts() {
        onBoardingRepository.getProducts()
    }


}

