package com.android.alchemy.usecase

import com.android.alchemy.data.remote.model.response.ProductResponse
import com.android.kotlinmvvm.data.database.entity.Product

interface LoginUseCase {

    fun getProducts()

}