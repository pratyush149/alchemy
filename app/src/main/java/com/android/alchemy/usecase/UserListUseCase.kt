package com.android.alchemy.usecase

import com.android.alchemy.data.remote.model.request.ProductRequest

interface UserListUseCase {

    fun fetchUsers(body : ProductRequest)
}