package com.android.alchemy.common

import android.app.Activity
import android.content.Context
import android.view.inputmethod.InputMethodManager

class KeyboardUtils {
    companion object{
        fun hideSoftKeyboard(context: Context) {
            val inputManager = (context as Activity).getSystemService(
                Context.INPUT_METHOD_SERVICE
            ) as InputMethodManager
            val focusedView = context.currentFocus
            /*
             * If no view is focused, an NPE will be thrown
             */
            if (focusedView != null) {
                inputManager.hideSoftInputFromWindow(
                    focusedView.windowToken,
                    InputMethodManager.HIDE_NOT_ALWAYS
                )
            }
        }
    }

}