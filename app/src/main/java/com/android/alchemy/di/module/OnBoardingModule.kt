package com.android.alchemy.di.module

import com.android.alchemy.data.remote.service.UsersService
import com.android.alchemy.data.repository.implementor.OnBoardingRepositoryImpl
import com.android.alchemy.data.repository.implementor.OnBoardingRepositoryRemoteImpl
import com.android.alchemy.data.repository.interactor.OnBoardingRepository
import com.android.alchemy.di.PerActivity
import com.android.alchemy.presentation.login.MainViewModelFactory
import com.ee.core.networking.Scheduler
import dagger.Module
import dagger.Provides
import io.reactivex.disposables.CompositeDisposable

@Module
@PerActivity
class OnBoardingModule {

    @Provides
    fun compositeDisposable(): CompositeDisposable = CompositeDisposable()

    @Provides
    @PerActivity
    fun provideLoginViewModelFactory(
        repository: OnBoardingRepository.Repository,
        compositeDisposable: CompositeDisposable
    ): MainViewModelFactory = MainViewModelFactory(repository, compositeDisposable)

    @Provides
    @PerActivity
    fun provideOnBoardingRepository(
        remote: OnBoardingRepository.Remote, scheduler: Scheduler,
        compositeDisposable: CompositeDisposable
    ): OnBoardingRepository.Repository =
        OnBoardingRepositoryImpl(remote, scheduler, compositeDisposable)

    @Provides
    @PerActivity
    fun provideOnBoardingRemote(usersService: UsersService): OnBoardingRepository.Remote =
        OnBoardingRepositoryRemoteImpl(usersService)

}