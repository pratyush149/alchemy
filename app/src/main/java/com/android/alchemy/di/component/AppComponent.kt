package com.android.alchemy.di.component

import com.android.alchemy.UsersApplication
import com.android.alchemy.common.SharedPreferenceHelper
import com.android.alchemy.data.remote.service.UsersService
import com.android.alchemy.di.AppScope
import com.android.alchemy.di.module.AppModule
import com.android.alchemy.di.module.OnBoardingModule
import com.android.alchemy.di.module.UserListModule
import com.android.alchemy.presentation.base.BaseActivity
import com.ee.core.di.CoreComponent
import dagger.Component

@AppScope
@Component(dependencies = [CoreComponent::class], modules = [AppModule::class])
interface AppComponent {

    fun sharedPreferenceHelper(): SharedPreferenceHelper
    fun userService(): UsersService


    fun inject(application: UsersApplication)

    fun inject(baseActivity: BaseActivity)

    // sub-components
    fun addOnBoardingComponent(onBoardingModule: OnBoardingModule): OnBoardingComponent
    fun addUserListComponent(userListModule: UserListModule): UserListComponent


}