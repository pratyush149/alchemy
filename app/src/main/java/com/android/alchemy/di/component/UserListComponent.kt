package com.android.alchemy.di.component


import com.android.alchemy.di.PerActivity
import com.android.alchemy.di.module.UserListModule
import com.android.alchemy.presentation.userlist.AddProductActivity
import dagger.Subcomponent


@PerActivity
@Subcomponent(modules = [UserListModule::class])
interface UserListComponent {

    fun inject(userListActivity: AddProductActivity)
}