package com.android.alchemy.di.module

import com.android.alchemy.data.remote.service.UsersService
import com.android.alchemy.data.repository.implementor.AddProductRepositoryImpl
import com.android.alchemy.data.repository.implementor.AddProductRepositoryRemoteImpl
import com.android.alchemy.data.repository.interactor.AddProductRepository
import com.android.alchemy.di.PerActivity
import com.android.alchemy.presentation.userlist.AddProductViewModelFactory
import com.ee.core.networking.Scheduler
import dagger.Module
import dagger.Provides
import io.reactivex.disposables.CompositeDisposable

@PerActivity
@Module
class UserListModule {

    @Provides
    fun compositeDisposable(): CompositeDisposable = CompositeDisposable()

    @Provides
    @PerActivity
    fun provideUsersListViewModelFactory(
        repository: AddProductRepository.Repository,
        compositeDisposable: CompositeDisposable
    ): AddProductViewModelFactory = AddProductViewModelFactory(repository, compositeDisposable)

    @Provides
    @PerActivity
    fun provideUsersListRepository(
        remote: AddProductRepository.Remote, scheduler: Scheduler,
        compositeDisposable: CompositeDisposable
    ): AddProductRepository.Repository =
        AddProductRepositoryImpl(remote, scheduler, compositeDisposable)

    @Provides
    @PerActivity
    fun provideUsersListRemote(usersService: UsersService): AddProductRepository.Remote =
        AddProductRepositoryRemoteImpl(usersService)

}