package com.android.alchemy.di.component


import com.android.alchemy.di.PerActivity
import com.android.alchemy.di.module.OnBoardingModule
import com.android.alchemy.presentation.login.MainActivity
import dagger.Subcomponent

@PerActivity
@Subcomponent(modules = [OnBoardingModule::class])
interface OnBoardingComponent {

    fun inject(loginActivity: MainActivity)
}