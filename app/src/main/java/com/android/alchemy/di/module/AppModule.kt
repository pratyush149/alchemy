package com.android.alchemy.di.module

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import com.android.alchemy.common.SharedPreferenceHelper
import com.android.alchemy.data.remote.service.UsersService
import com.android.alchemy.di.AppScope
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import javax.inject.Singleton

@Module
@AppScope
class AppModule {

    @AppScope
    @Provides
    fun provideSharedPref(sharedPreferences: SharedPreferences): SharedPreferenceHelper =
        SharedPreferenceHelper(sharedPreferences)

    @AppScope
    @Provides
    fun smartFarmingService(retrofit: Retrofit): UsersService =
        retrofit.create(UsersService::class.java)

}