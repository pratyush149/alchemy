package com.android.alchemy.data.repository.interactor

import com.android.alchemy.data.remote.model.request.ProductRequest
import com.android.alchemy.data.remote.model.response.ProductResponse
import com.ee.core.networking.Outcome
import io.reactivex.Flowable
import io.reactivex.subjects.PublishSubject

interface AddProductRepository {

    interface Repository {

        val userListResponse: PublishSubject<Outcome<ProductResponse>>

        fun addProduct(body: ProductRequest)
    }

    interface Remote {

        fun addProduct(body: ProductRequest): Flowable<ProductResponse>
    }

}