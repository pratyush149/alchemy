package com.android.alchemy.data.repository.implementor

import com.android.alchemy.data.remote.model.response.ProductResponse
import com.android.alchemy.data.repository.interactor.OnBoardingRepository
import com.android.kotlinmvvm.data.database.ProductDatabase
import com.android.kotlinmvvm.data.database.dao.ProductDao
import com.ee.core.extension.*
import com.ee.core.networking.Outcome
import com.ee.core.networking.Scheduler
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.subjects.PublishSubject

class OnBoardingRepositoryImpl(
    private val remote: OnBoardingRepository.Remote,
    private val scheduler: Scheduler,
    private val compositeDisposable: CompositeDisposable
) : OnBoardingRepository.Repository {

    override val productResponse: PublishSubject<Outcome<List<ProductResponse>>> by lazy {
        PublishSubject.create<Outcome<List<ProductResponse>>>()
    }



    override fun getProducts() {
        productResponse.loading(true)

        remote.getProducts()
            .performOnBackOutOnMain(scheduler)
            .subscribe({ data ->
                productResponse.success(data)
            }, { error ->
                productResponse.failed(error)
            })
            .addTo(compositeDisposable)
    }



}