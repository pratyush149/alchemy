package com.android.alchemy.data.remote.service

import com.android.alchemy.data.remote.model.request.ProductRequest
import com.android.alchemy.data.remote.model.response.ProductResponse
import io.reactivex.Flowable
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST


/**
 * All API calls will be handled from here
 */
interface UsersService {

    @GET("products")
    fun getProducts(): Flowable<List<ProductResponse>>

    @POST("products")
    fun addProduct(@Body request: ProductRequest): Flowable<ProductResponse>

    /*@POST("api/login")
    fun doLogin(@Body request: LoginRequest): Flowable<LoginResponse>*/


}

