package com.android.alchemy.data.remote.model.response

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_EMPTY)
data class ProductResponse(
    @JsonProperty("id")
    val id: String?,
    @JsonProperty("createdAt")
    val createdAt: String?,
    @JsonProperty("name")
    val name: String?,
    @JsonProperty("color")
    val color: String?,
    @JsonProperty("price")
    val price: String?,
    @JsonProperty("adjective")
    val adjective: String?,
    @JsonProperty("material")
    val material: String?,
    @JsonProperty("image")
    val image: String?


)

