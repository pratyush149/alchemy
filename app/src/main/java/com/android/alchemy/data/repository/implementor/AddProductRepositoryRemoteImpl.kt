package com.android.alchemy.data.repository.implementor

import com.android.alchemy.data.remote.model.request.ProductRequest
import com.android.alchemy.data.remote.model.response.ProductResponse
import com.android.alchemy.data.remote.service.UsersService
import com.android.alchemy.data.repository.interactor.AddProductRepository
import io.reactivex.Flowable

class AddProductRepositoryRemoteImpl(private val usersService: UsersService) :
    AddProductRepository.Remote {


    override fun addProduct(body: ProductRequest): Flowable<ProductResponse> {
        return usersService.addProduct(body)
    }
}