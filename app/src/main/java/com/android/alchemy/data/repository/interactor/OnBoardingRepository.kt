package com.android.alchemy.data.repository.interactor

import com.android.alchemy.data.remote.model.response.ProductResponse
import com.ee.core.networking.Outcome
import io.reactivex.Flowable
import io.reactivex.subjects.PublishSubject

interface OnBoardingRepository {

    interface Repository {

        val productResponse: PublishSubject<Outcome<List<ProductResponse>>>

        fun getProducts()

    }

    interface Remote {

        fun getProducts(): Flowable<List<ProductResponse>>
    }

}