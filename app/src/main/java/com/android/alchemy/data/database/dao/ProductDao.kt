package com.android.kotlinmvvm.data.database.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.android.alchemy.data.remote.model.response.ProductResponse
import com.android.kotlinmvvm.data.database.entity.Product

@Dao
interface ProductDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun setProducts(productList: List<Product>)

    @Query("SELECT * from product_table")
    fun getProducts() : LiveData<List<Product>>

    @Query("DELETE FROM product_table")
    fun deleteAll()

}