package com.android.alchemy.data.repository.implementor

import com.android.alchemy.data.remote.model.request.ProductRequest
import com.android.alchemy.data.remote.model.response.ProductResponse
import com.android.alchemy.data.repository.interactor.AddProductRepository
import com.ee.core.extension.*
import com.ee.core.networking.Outcome
import com.ee.core.networking.Scheduler
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.subjects.PublishSubject

class AddProductRepositoryImpl(
    private val remote: AddProductRepository.Remote,
    private val scheduler: Scheduler,
    private val compositeDisposable: CompositeDisposable
) : AddProductRepository.Repository {

    override val userListResponse: PublishSubject<Outcome<ProductResponse>> by lazy {
        PublishSubject.create<Outcome<ProductResponse>>()
    }

    override fun addProduct(body: ProductRequest) {
        userListResponse.loading(true)

        remote.addProduct(body)
            .performOnBackOutOnMain(scheduler)
            .subscribe({ data ->
                userListResponse.success(data)
            }, { error ->
                userListResponse.failed(error)
            })
            .addTo(compositeDisposable)
    }
}