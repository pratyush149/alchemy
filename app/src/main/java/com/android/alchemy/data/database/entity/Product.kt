package com.android.kotlinmvvm.data.database.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "product_table")
data class Product(

    @PrimaryKey(autoGenerate = false)
    @ColumnInfo(name = "id")
    var id: String,

    @ColumnInfo(name = "createdAt")
    var createdAt: String,

    @ColumnInfo(name = "name")
    var name: String,

    @ColumnInfo(name = "color")
    var color: String,

    @ColumnInfo(name = "price")
    var price: String,

    @ColumnInfo(name = "adjective")
    var adjective: String,


    @ColumnInfo(name = "material")
    var material: String,

    @ColumnInfo(name = "image")
    var image: String

)