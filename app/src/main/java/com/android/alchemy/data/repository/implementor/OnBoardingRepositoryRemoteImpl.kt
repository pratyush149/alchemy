package com.android.alchemy.data.repository.implementor

import com.android.alchemy.data.remote.model.response.ProductResponse
import com.android.alchemy.data.remote.service.UsersService
import com.android.alchemy.data.repository.interactor.OnBoardingRepository
import io.reactivex.Flowable

class OnBoardingRepositoryRemoteImpl(private val usersService: UsersService) :
    OnBoardingRepository.Remote {

    override fun getProducts(): Flowable<List<ProductResponse>> {
        return usersService.getProducts()
    }
}