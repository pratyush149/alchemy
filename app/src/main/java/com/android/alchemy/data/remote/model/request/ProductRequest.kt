package com.android.alchemy.data.remote.model.request

data class ProductRequest(
    val name: String,
    val color: String,
    val price: String,
    val adjective: String,
    val material: String
)

